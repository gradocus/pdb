#include "node.h"

#include <cstdlib>

node::node()
{
    this->buf = NULL;
}

void node::reset()
{
    this->close();
    this->open();
}
