#include "parser.h"

node* parser::BuildTree(char* query)
{
    int end;
    return parser::BuildTree(query, 0, end);
}

node* parser::BuildTree(char* query, int fromIndex, int &toIndex)
{
    int i = fromIndex;
    node* root;

    int opId = 0;
    char* operation = new char[10];
    while (i + opId < strlen(query) && query[i + opId] != '(')
    {
        if (query[i + opId] == ',' || query[i + opId] == ' ')
            ++i;
        else
            operation[opId++] = query[i + opId];
    }
    operation[opId++] = '\0';

    i += opId;

    if (strcmp(operation, "scan") == 0)
    {
        int argId = 0;
        bool readAlias = false;
        char* argument = new char[16];
        char* alias = new char[16];
        alias[0] = '\0';
        while (i + argId < strlen(query) && query[i + argId] != ')')
        {
            if (query[i + argId] == ' ')
            {
                argument[argId++] = '\0';
                i += 3 + argId;
                argId = 0;
                readAlias = true;
                continue;
            }
            if (readAlias)
                alias[argId++] = query[i + argId];
            else
                argument[argId++] = query[i + argId];
        }
        if (strlen(alias) == 0)
            argument[argId++] = '\0';
        else
            alias[argId++] = '\0';
        toIndex = i + argId;
        if (strlen(alias) == 0)
            root = new scan(argument);
        else
            root = new scan(argument, alias);
    }
    else if (strcmp(operation, "project") == 0)
    {
        node* innerNode = parser::BuildTree(query, i, i);
        ++i;

        int argId = 0;
        char* argument = new char[16];
        vector<char*> arguments;
        while (i + argId < strlen(query) && query[i + argId] != ')')
        {
            if (query[i + argId] == ' ')
                ++i;
            else if (query[i + argId] == ',')
            {
                argument[argId++] = '\0';
                arguments.push_back(argument);
                argument = new char[16];
                i += argId; argId = 0;
            }
            else
                argument[argId++] = query[i + argId];
        }
        if (argId != 0)
        {
            argument[argId++] = '\0';
            arguments.push_back(argument);
        }
        toIndex = i + argId;

        root = new project(arguments);
        root->left = innerNode;
    }
    else if (strcmp(operation, "restrict") == 0)
    {
        node* innerNode = parser::BuildTree(query, i, i);
        ++i;

        int argId = 0;
        char* argument = new char[16];
        vector<char*> arguments;
        bool parse_condition = false;
        while (i + argId < strlen(query) && query[i + argId] != ')')
        {
            if (query[i + argId] == ' ')
                ++i;
            else if ((!parse_condition && (query[i + argId] == '<' || query[i + argId] == '>' || query[i + argId] == '='))
                || (parse_condition && query[i + argId] != '<' && query[i + argId] != '>' && query[i + argId] != '='))
            {
                argument[argId] = '\0';
                arguments.push_back(argument);
                argument = new char[16];
                i += argId; argId = 0;
                argument[argId++] = query[i + argId];
                parse_condition = !parse_condition;
            }
            else
                argument[argId++] = query[i + argId];
        }
        if (argId != 0)
        {
            argument[argId++] = '\0';
            arguments.push_back(argument);
        }
        toIndex = i + argId;

        root = new restrict(arguments[0], arguments[1], arguments[2]);
        root->left = innerNode;
    }
    else if (strcmp(operation, "join") == 0)
    {
        int argId = 0;
        node* leftNode = parser::BuildTree(query, i, i);
        ++i;
        char* leftArgumentName = new char[16];

        while (i + argId < strlen(query) && query[i + argId] != ',')
        {
            if (query[i + argId] == ' ')
                ++i;
            else
                leftArgumentName[argId++] = query[i + argId];
        }
        leftArgumentName[argId] = '\0';

        i += argId; argId = 0;
        node* rightNode = parser::BuildTree(query, i, i);
        ++i;
        char* rightArgumentName = new char[16];

        while (i + argId < strlen(query) && query[i + argId] != ')')
        {
            if (query[i + argId] == ' ')
                ++i;
            else
                rightArgumentName[argId++] = query[i + argId];
        }
        if (argId != 0)
            rightArgumentName[argId++] = '\0';

        toIndex = i + argId;

        root = new join(leftArgumentName, rightArgumentName);
        root->left = new exchange(leftArgumentName);
        root->left->left = leftNode;
//        root->right = new exchange(rightArgumentName);
//        root->right->left = rightNode;
        root->right = new cache();
        root->right->left = new exchange(rightArgumentName);
        root->right->left->left = rightNode;
    }
    else
    {
        root = NULL;
    }

    if (root != NULL && fromIndex == 0)
    {
        node* finalExchange = new exchange((char*)"", true);
        finalExchange->left = root;
        return finalExchange;
    }

    return root;
}
