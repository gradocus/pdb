#ifndef GATHER_H
#define GATHER_H

#include "node.h"

#include "mpi.h"
#include <vector>

class gather: public node
{
    int rank;
    int size;
    int port;
    MPI_Request* requests;
    int* recv;
    unsigned int eofCount;

    node* sourceNode;

    void init();
public:
//    gather();
    gather(int port);

    void open();
    void next();
    void close();
};

#endif
