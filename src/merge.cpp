#include "merge.h"

merge::merge()
{
    
}

void merge::open()
{
    if (this->left != NULL)
    {
        this->gatherNode = this->left;
    }
    else
    {
        cerr << "Error: merge has not an any gather node." << endl;
         // TODO: Add exception.
         throw new exception();
    }

    if (this->right != NULL)
    {
        this->splitterNode = this->right;
    }
    else
    {
        cerr << "Error: merge has not an any splitter node." << endl;
        // TODO: Add exception.
        throw new exception();
    }

    this->gatherNode->open();
    this->splitterNode->open();
}

static bool flag = false;
void merge::next()
{
    while(true)
    {
        flag = !flag;
        if (flag)
        {
            this->gatherNode->next();
            if (this->gatherNode->buf != NULL && !this->gatherNode->buf->IsEOF())
            {
                this->buf = this->gatherNode->buf;
                return;
            }
        }
        else
        {
            this->splitterNode->next();
            if (!this->splitterNode->buf->IsEOF())
            {
                this->buf = this->splitterNode->buf;
                return;
            }
        }

        if (this->gatherNode->buf != NULL && this->gatherNode->buf->IsEOF()
            && this->splitterNode->buf != NULL && this->splitterNode->buf->IsEOF())
        {
            if (this->buf == NULL || !this->buf->IsEOF())
            {
                this->buf = this->splitterNode->buf;
            }
            return;
        }
    }
}

void merge::close()
{
    this->splitterNode->close();
    this->gatherNode->close();
}
