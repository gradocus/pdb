#include "restrict.h"

restrict::restrict()
{
    
}

restrict::restrict(char* firstArgument, char* condition, char* secondArgument)
{
    this->firstArgument = firstArgument;
    this->condition = condition;
    this->secondArgument = secondArgument;
}

void restrict::open()
{
    if (this->left != NULL)
    {
         this->sourceNode = this->left;
    }
    else if (this->right != NULL)
    {
        this->sourceNode = this->right;
    }
    else
    {
        cerr << "Error: restrict has not found an any source node." << endl;
        // TODO: Add restrict exception.
        throw new exception();
    }

    this->sourceNode->open();
}

void restrict::next()
{
    this->sourceNode->next();

    int first = -1; int second = -1;
    while (!this->sourceNode->buf->IsEOF())
    {
        if (this->firstArgument[0] > '9' || this->firstArgument[0] < '0') // is not number.
            first = (*this->sourceNode->buf)[this->firstArgument];
        else
            first = atoi(this->firstArgument);

        if (this->secondArgument[0] > '9' || this->secondArgument[0] < '0') // is not number.
            second = (*this->sourceNode->buf)[this->secondArgument];
        else
            second = atoi(this->secondArgument);

        if (((this->condition[0] == '=' || this->condition[1] == '=') && first == second)
            || (this->condition[0] == '>' && first > second)
            || (this->condition[0] == '<' && this->condition[1] != '>' && first < second)
            || (this->condition[0] == '<' && this->condition[1] == '>' && first != second)
           )
            break;
        this->sourceNode->next();
    }

    this->buf = this->sourceNode->buf;
}

void restrict::close()
{
    this->sourceNode->close();
}
