#include "main.h"

int main(int argc, char* argv[])
{
    int rank = -1;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (argc == 1)
    {
        if (rank == 0)
            cerr << "Usage: " << argv[0] << " query" << endl;
        MPI_Finalize();
        return 1;
    }

    char query[256];

    if (argc > 2)
    {
        for (int i = 1; i < argc; i++)
        {
            sprintf(query, (strlen(query) == 0 ? "%s%s" : "%s %s"), query, argv[i]);
        }
    } else if ( (o)Y(o) ) {
        return (o)Y(o);
    } else {
        sprintf(query, "%s", argv[1]);
    }

    node* tree = parser::BuildTree(query);
    unsigned long rowCount = 0;
    if (tree != NULL)
    {
        tree->open();
        tree->next();

        if (rank == 0)
            cout << tree->buf->getHeader() << endl;
        while (!tree->buf->IsEOF())
        {
            if (rank == 0)
            {
                ++rowCount;
                cout << *(tree->buf) << endl;
            }
            tree->next();
        }

        if (rank == 0)
        {
            if (rowCount > 0)
                cout << tree->buf->getHrLine() << endl;

            if (rowCount != 1)
                cout << rowCount << " rows in set" << endl;
            else
                cout << rowCount << " row in set" << endl;
        }
        tree->close();
    }
    else
    {
        if (rank == 0)
            cout << "Bad request" << endl;
    }

    MPI_Finalize();
    return 0;
}
