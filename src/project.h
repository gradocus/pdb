#ifndef PROJECT_H
#define PROJECT_H

#include "node.h"

#include <vector>

class project: public node
{
    node* sourceNode;
    vector<char*> requiredAttributes;
public:
    project(vector<char*> requiredAttributes);

    void open();
    void next();
    void close();
};

#endif
