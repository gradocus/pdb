#include "splitter.h"

splitter::splitter(char* columnName, int port, bool zeroBased)
{
    this->init();

    this->columnName = columnName;
    this->port = port;
    this->zeroBased = zeroBased;
}

void splitter::init()
{
    MPI_Comm_rank(MPI_COMM_WORLD, &(this->rank));
    MPI_Comm_size(MPI_COMM_WORLD, &(this->size));
}

void splitter::open()
{
    if (this->left != NULL)
    {
         this->sourceNode = this->left;
    }
    else
    {
        cerr << "Error: split has not an any source node." << endl;
        // TODO: Add source not found exception.
        throw new exception();
    }

    this->sourceNode->open();
}

int splitter::getDestinationProcess(int value)
{
    return value % this->size;
}

void splitter::next()
{
    this->sourceNode->next();

    while (!this->sourceNode->buf->IsEOF())
    {
        int dest = 0;
        if (!this->zeroBased)
            dest = getDestinationProcess((*this->sourceNode->buf)[this->columnName]);
        if (this->rank != dest)
        {
//cout << rank << ":" << port << " ";
            this->sourceNode->buf->sendTo(dest, this->port);
        }
        else
        {
            this->buf = this->sourceNode->buf;
            return;
        }

        this->sourceNode->next();
    }

    if (this->buf == NULL || !this->buf->IsEOF())
    {
        this->buf = this->sourceNode->buf;
        for (int i = 0; i < this->size; ++i)
        {
//cout << rank << ":" << port << " ";
            this->buf->sendTo(i, this->port);
        }
    }
}

void splitter::close()
{
    this->sourceNode->close();
}
