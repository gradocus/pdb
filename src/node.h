#ifndef NODE_H
#define NODE_H

#include <vector>
#include <iostream> // cout/cerr
using namespace std;

#include "tuple.h"

class node
{
public:
    node();

    tuple* buf;

    virtual void open() = 0;
    virtual void next() = 0;
    virtual void close() = 0;

    void reset();

    node *left;
    node *right;
};

#endif
