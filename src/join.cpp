#include "join.h"

join::join()
{
    
}

join::join(char* leftArgumentName, char* rightArgumentName)
{
    this->leftArgumentName = leftArgumentName;
    this->rightArgumentName = rightArgumentName;
}

void join::open()
{
    this->left->open();
    this->right->open();
}

void join::next()
{
    if (this->left->buf == NULL)
        this->left->next();

    while (!this->left->buf->IsEOF())
    {
        this->right->next();
        while (!this->right->buf->IsEOF())
        {
            if ((*this->left->buf)[this->leftArgumentName] == (*this->right->buf)[this->rightArgumentName])
            {
                this->buf = this->left->buf->join(this->right->buf);
                return;
            }
            this->right->next();
        }
        this->right->reset();

        this->left->next();
    }
    if (this->buf == NULL || !this->buf->IsEOF())
    {
        this->right->next();
        this->buf = this->left->buf->join(this->right->buf);
        this->right->close();
    }
}

void join::close()
{
    this->left->close();
    this->right->close();
}
