#ifndef MERGE_H
#define MERGE_H

#include "node.h"

#include <vector>

class merge: public node
{
    node* gatherNode;
    node* splitterNode;
public:
    merge();

    void open();
    void next();
    void close();
};

#endif
