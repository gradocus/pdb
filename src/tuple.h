#ifndef TUPLE_H
#define TUPLE_H

#include "mpi.h"
#include <stdio.h> // sprintf
#include <vector>
#include <string.h> // For strcmp.
#include <iostream> // cout/cerr
using namespace std;

class tuple
{
public:
    tuple(); // Empty tuple.
    tuple(tuple* t);
    tuple(vector<char*> columnNames); // EOF tuple.
    tuple(vector<char*> columnNames, vector<int> values);
protected:
    vector <int> values;
    vector <char*> columnNames;

   int& getColumnValue(const char* columnName);
public:
    bool operator!=(const tuple &t);
    tuple operator=(const tuple &t);
    int& operator[] (const char* columnName);
    friend ostream& operator<<(ostream& out, const tuple& t);

    char* getHeader();
    char* getHrLine();
    tuple* join(tuple* t);
    tuple* project(vector<char*>* requiredAttributes);
    void sendTo(int dest, int port);
//    bool isEmpty();
    bool IsEOF();
};

#endif
