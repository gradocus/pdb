#include "cache.h"

cache::cache()
{
    this->index = -1;
    this->sourceNode = NULL;
}

void cache::open()
{
    this->index = 0;

    if (this->sourceNode == NULL)
    {
        if (this->left != NULL)
        {
             this->sourceNode = this->left;
        }
        else if (this->right != NULL)
        {
            this->sourceNode = this->right;
        }
        else
        {
            cerr << "Error: cache has not an any source node." << endl;
            // TODO: Add source not found exception.
            throw new exception();
        }

        this->sourceNode->open();
    }

    if (this->tuples.size() == 0)
    {
        do
        {
        this->sourceNode->next();
            this->tuples.push_back(new tuple(this->sourceNode->buf));
        }
        while(!this->sourceNode->buf->IsEOF());
        this->sourceNode->close();
    }
}

void cache::next()
{
    if (this->index < this->tuples.size())
    {
        this->buf = this->tuples[this->index++];
    }
}

void cache::close()
{
    this->index = -1;
}
