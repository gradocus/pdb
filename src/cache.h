#ifndef CACHE_H
#define CACHE_H

#include <vector>

#include "node.h"

class cache: public node
{
    int index;
    vector<tuple*> tuples;

    node* sourceNode;
public:
    cache();

    void open();
    void next();
    void close();
};

#endif
