#include "gather.h"

gather::gather(int port)
{
    this->init();
    this->port = port;
}

void gather::init()
{
    MPI_Comm_rank(MPI_COMM_WORLD, &(this->rank));
    MPI_Comm_size(MPI_COMM_WORLD, &(this->size));

    this->requests = new MPI_Request[this->size];
    this->recv = new int[this->size];
}

void gather::open()
{
    this->buf = NULL;
    this->eofCount = 0;
    for (int i = 0; i < this->size; ++i)
    {
        this->recv[i] = -1;
        MPI_Irecv(&this->recv[i], 1, MPI_INT, i, this->port, MPI_COMM_WORLD, &this->requests[i]);
    }
}

void gather::next()
{
    if (this->buf != NULL)
    {
        if (this->buf->IsEOF())
            return;
        this->buf = NULL;
    }

    int isReceived;
    for (int i = 0; i < this->size; ++i)
    {
        MPI_Test(&this->requests[i], &isReceived, MPI_STATUS_IGNORE);
        if (isReceived && this->recv[i] != -1)
        {
            int received = this->recv[i];
//cout << rank << ":" << port << " received '" << received << "' from " << i << ":" << port << endl;
            this->recv[i] = -1;

            if (received == 0)
            {
                this->eofCount++;
//                MPI_Irecv(&this->recv[i], 1, MPI_INT, i, this->port, MPI_COMM_WORLD, &this->requests[i]);
                continue;
            }

            // TODO: Add tuple reading (recv).
            vector<char*> columnNames;
            for (int j = 0; j < received; ++j)
            {
                char* buff = new char[255];
                MPI_Recv(buff, 255, MPI_CHAR, i, this->port, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                columnNames.push_back(buff);
            }

            int* values = new int[received];
            MPI_Recv(values, received, MPI_INT, i, this->port, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

            vector<int> v(values, values + received);
            this->buf = new tuple(columnNames, v);

            MPI_Irecv(&this->recv[i], 1, MPI_INT, i, this->port, MPI_COMM_WORLD, &this->requests[i]);
            return;
        }
    }

    if (this->eofCount == this->size)
    {
        this->buf = new tuple();
    }
}

void gather::close()
{
}
