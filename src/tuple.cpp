#include "tuple.h"

tuple::tuple()
{ }

tuple::tuple(tuple* t)
{
    this->columnNames = t->columnNames;
    this->values = t->values;
}

tuple::tuple(vector<char*> columnNames)
{
    for (int i = 0; i < columnNames.size() - 1; ++i)
    {
        for (int j = i + 1; j < columnNames.size(); ++j)
        {
            if (strcmp(columnNames[i], columnNames[j]) == 0)
            {
                // TODO: Add non unique column name exception.
                throw new exception();
            }
        }
    }

    this->columnNames = columnNames;
}

tuple::tuple(vector<char*> columnNames, vector<int> values)
{
    for (int i = 0; i < columnNames.size() - 1; ++i)
    {
        for (int j = i + 1; j < columnNames.size(); ++j)
        {
            if (strcmp(columnNames[i], columnNames[j]) == 0)
            {
                // TODO: Add non unique column name exception.
                throw new exception();
            }
        }
    }

    this->columnNames = columnNames;
    this->values = values;
}

bool tuple::IsEOF()
{
    return this->values.size() == 0 /*&& this->columnNames.size() != 0*/;
}

char* tuple::getHeader()
{
    char* result = new char[255 * 3];

    char* hrLine = new char[255];
    char* header = new char[255];
    sprintf(hrLine, "+");
    sprintf(header, "|");
    for (int i = 0; i < this->columnNames.size(); ++i)
    {
        int columnNameSize = strlen(this->columnNames[i]);
        for (int j = 0; j < columnNameSize - 8; ++j)
            sprintf(hrLine, "%s-", hrLine);
        sprintf(hrLine, "%s--------+", hrLine);
        sprintf(header, "%s%8s|", header, this->columnNames[i]);
    }
    sprintf(result, "%s\n%s\n%s", hrLine, header, hrLine);
    return result;
}

char* tuple::getHrLine()
{
    char* hrLine = new char[255];

    sprintf(hrLine, "+");
    for (int i = 0; i < this->columnNames.size(); ++i)
    {
        int columnNameSize = strlen(this->columnNames[i]);
        for (int j = 0; j < columnNameSize - 8; ++j)
            sprintf(hrLine, "%s-", hrLine);
        sprintf(hrLine, "%s--------+", hrLine);
    }

    return hrLine;
}

tuple* tuple::join(tuple* t)
{
    vector<char*> colNames;

    colNames.insert(colNames.end(), this->columnNames.begin(), this->columnNames.end());
    colNames.insert(colNames.end(), t->columnNames.begin(), t->columnNames.end());

    if (!this->IsEOF())
    {
        vector<int> val;

        val.insert(val.end(), this->values.begin(), this->values.end());
        val.insert(val.end(), t->values.begin(), t->values.end());

        return new tuple(colNames, val);
    }

    return new tuple(colNames);
}

tuple* tuple::project(vector<char*>* requiredAttributes)
{
    if (this->IsEOF())
    {
        return new tuple(*requiredAttributes);
    }

    vector<int> newVector;
    for (vector<char*>::iterator i = requiredAttributes->begin();
        i != requiredAttributes->end(); ++i)
    {
        newVector.push_back(this->getColumnValue(*i));
    }
    return new tuple(*requiredAttributes, newVector);
}

void tuple::sendTo(int dest, int port)
{
    MPI_Request request;
//cout << "send " << *this << " to " << dest << ":" << port << endl;

    int* b = new int[1];
    if (this->IsEOF())
    {
        b[0] = 0;
        MPI_Isend(b, 1, MPI_INT, dest, port, MPI_COMM_WORLD, &request);
        return;
    }
    b[0] = this->columnNames.size();
    MPI_Isend(b, 1, MPI_INT, dest, port, MPI_COMM_WORLD, &request);

    char* buff = new char[255];
    for (int i = 0; i < b[0]; ++i)
    {
        sprintf(buff, "%s", this->columnNames[i]);
        MPI_Isend(buff, 255, MPI_CHAR, dest, port, MPI_COMM_WORLD, &request);
    }

    int* values = new int[b[0]];
    for (int i = 0; i < b[0]; ++i)
    {
        values[i] = this->values[i];
    }
    MPI_Isend(values, b[0], MPI_INT, dest, port, MPI_COMM_WORLD, &request);
}

bool tuple::operator!=(const tuple &t)
{
    return t.values != this->values;
}

int& tuple::operator[](const char* columnName)
{
    return this->getColumnValue(columnName);
}

int& tuple::getColumnValue(const char* columnName)
{
    int colId = -1;
    bool columnNameContainsDot = strchr(columnName, '.') == NULL;
    for (int j = 0; j < columnNames.size(); ++j)
    {
        if (strcmp(
                (columnNameContainsDot ? strchr(columnNames[j], '.') + 1
                    : columnNames[j]), columnName) == 0)
        {
            if (colId != -1)
            {
                // TODO: Add exception.
                cerr << "Error: column name `" << columnName << "` is not unique." << endl;
                throw new exception();
            }
            colId = j;
        }
    }
    if (colId == -1) // TODO: Add exception.
    {
        cerr << "Error: column [" << columnName << "] is not found." << endl;
        throw new exception();
    }

    return this->values[colId];
}

ostream& operator<<(ostream& out, const tuple& t)
{
    int tupleSize = t.values.size();
    if (tupleSize > 0)
        out << "|";
    else
       out << "EOF";
    for (int i = 0; i < tupleSize; ++i)
    {
        out.width(strlen(t.columnNames[i]) < 8 ? 8 : strlen(t.columnNames[i]));
        out << t.values[i] << "|";
    }
    return out;
}
