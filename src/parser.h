#ifndef PARSER_H
#define PARSER_H

#include <string.h> // For strcmp.
#include <iostream> // cout/cerr

#include "node.h"

#include "scan.h"
#include "project.h"
#include "restrict.h"
#include "cache.h"
#include "join.h"
#include "exchange.h"

class parser
{
    static node* BuildTree(char* query, int fromIndex, int &toIndex);
public:
    static node* BuildTree(char* query);
};

#endif
