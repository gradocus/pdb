#ifndef RESTRICT_H
#define RESTRICT_H

#include <cstdlib>  // atoi

#include "node.h"

#include <vector>
#include <exception>

class restrict: public node
{
    restrict();

    char* firstArgument;
    char* condition;
    char* secondArgument;

    node* sourceNode;
public:
    restrict(char*, char*, char*);

    void open();
    void next();
    void close();
};

#endif
