#include "exchange.h"

static int counter = 0;

exchange::exchange(char* columnName, bool zeroBased)
{
    this->mergeNode = new merge();

    this->mergeNode->left = new gather(counter);
    this->mergeNode->right = new splitter(columnName, counter, zeroBased);
    counter++;
}

void exchange::open()
{
    if (this->left != NULL)
    {
         this->mergeNode->right->left = this->left; // splitter->left = this->left;
    }
    else if (this->right != NULL)
    {
        this->mergeNode->right->left = this->right; // splitter->left = this->right;
    }
    else
    {
        cerr << "Error: exchange has not an any source node." << endl;
        // TODO: Add source not found exception.
        throw new exception();
    }

    this->mergeNode->open();
}

void exchange::next()
{
    this->mergeNode->next();
    this->buf = this->mergeNode->buf;
}

void exchange::close()
{
    this->mergeNode->close();
}
