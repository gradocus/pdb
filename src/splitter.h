#ifndef SPLITTER_H
#define SPLITTER_H

#include "node.h"

#include "mpi.h"
#include <vector>

class splitter: public node
{
    int rank;
    int size;
    int port;

    char* columnName;
    bool zeroBased;
    node* sourceNode;

    void init();
public:
    splitter(char* columnName, int port, bool zeroBased);

    void open();
    void next();
    void close();
protected:
    int getDestinationProcess(int value);
};

#endif
