#ifndef SCAN_H
#define SCAN_H

#define DATA_FOLDER string("data")
#define MAX_COLUMN_NAME_LENGTH	16

#include <fstream> // ifstream (file reader).
#include <vector>
#include <string>  // lines from file.
using namespace std;

#include "mpi.h" // MPI

#include "tuple.h"
#include "node.h"

class scan: public node
{
    void init(char* fileName, char* tableName);

    string line;
    ifstream file;

    char* fileName;
    char* tableName;
    vector<char*> colNames;
public:
    scan(char* tableName);
    scan(char* fileName, char* tableName);

    void open();
    void next();
    void close();
};

#endif
