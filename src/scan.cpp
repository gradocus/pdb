#include "scan.h"

scan::scan(char* tableName)
{
    this->init(tableName, tableName);
}

scan::scan(char* fileName, char* tableName)
{
    this->init(fileName, tableName);
}

void scan::init(char* fileName, char* tableName)
{
    this->line = "";
    this->fileName = fileName;

    if (tableName[0] - '0' >= 0 && tableName[0] - '0' <= 9)
    {
        // TODO: Add exception.
        cerr << "The table name can't starts wint a number" << endl;
        throw new exception();
    }
    this->tableName = tableName;

    this->left = NULL;
    this->right = NULL;
}

void scan::open()
{
    char* pathToTableFile = new char[512];
    int rank = -1;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    sprintf(pathToTableFile, "data/%s_%d", this->fileName, rank);

    this->file.open(pathToTableFile);
    if (!this->file.is_open())
    {
        cerr << "Error: Can't open the `" << pathToTableFile << "` file" << endl;
        throw new exception();
    }

    getline(this->file, this->line);
  if (this->colNames.size() == 0)
  {
    char* colName = new char[MAX_COLUMN_NAME_LENGTH];

    strcpy(colName, this->tableName);
    strcat(colName, (&".")[0]);
    int colId = strlen(this->tableName) + 1;
    for (int i = 0; i < this->line.size(); ++i)
    {
        if (this->line[i] == ';')
        {
            colName[colId] = '\0';
            this->colNames.push_back(colName);
            colName = new char[MAX_COLUMN_NAME_LENGTH];

            strcpy(colName, this->tableName);
            strcat(colName, (&".")[0]);
            colId = strlen(this->tableName) + 1;
        }
        else
        {
            colName[colId++] = this->line[i];
        }
    }
    colName[colId] = '\0';
    this->colNames.push_back(colName);
  }
}

void scan::next()
{
    if (!this->file.eof())
    {
        getline(this->file, this->line);
        if (!this->file.eof())
        {
            vector<int> tupleVector;
            int value = 0;
            for (int i = 0; i < this->line.size(); ++i)
            {
                if (this->line[i] == ';')
                {
                    tupleVector.push_back(value);
                    value = 0;
                }
                else
                {
                    value = value * 10 + this->line[i] - '0';
                }
            }
            tupleVector.push_back(value);
            this->buf = new tuple(this->colNames, tupleVector);
        }
        else
        {
            if (!this->buf->IsEOF())
            {
                this->buf = new tuple(this->colNames);
            }
        }
    }
}

void scan::close()
{
    this->file.close();
}
