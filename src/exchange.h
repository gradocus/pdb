#ifndef EXCHANGE_H
#define EXCHANGE_H

#include "node.h"

#include "merge.h"
#include "gather.h"
#include "splitter.h"

#include <vector>

class exchange: public node
{
    node* mergeNode;
public:
    exchange(char* columnName, bool zeroBased = false);

    void open();
    void next();
    void close();
};

#endif
