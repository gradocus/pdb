#include "project.h"

project::project(vector<char*> requiredAttributes)
{
    this->requiredAttributes = requiredAttributes;
}

void project::open()
{
    if (this->left != NULL)
    {
         this->sourceNode = this->left;
    }
    else if (this->right != NULL)
    {
        this->sourceNode = this->right;
    }
    else
    {
        cerr << "Error: project has not an any source node." << endl;
        // TODO: Add source not found exception.
        throw new exception();
    }

    this->sourceNode->open();
}

void project::next()
{
    this->sourceNode->next();

    if (!this->sourceNode->buf->IsEOF())
        this->buf = this->sourceNode->buf->project(&this->requiredAttributes);
    else
        this->buf = new tuple(this->requiredAttributes);
}

void project::close()
{
    this->sourceNode->close();
}
