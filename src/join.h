#ifndef JOIN_H
#define JOIN_H

#include <vector>

#include "node.h"

class join: public node
{
    join();

    char* leftArgumentName;
    char* rightArgumentName;
public:
    join(char* leftArgumentName, char* rightArgumentName);

    void open();
    void next();
    void close();
};

#endif
