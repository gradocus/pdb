CC=mpic++
CFLAGS=-c
LDFLAGS=-Wall

SOURCES=$(wildcard src/*.cpp)
OBJECTS=$(patsubst src/%.cpp, lib/%.o, $(SOURCES))

TARGET=pdb


all: $(SOURCES) $(TARGET)

$(TARGET): lib $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

lib:
	mkdir lib

lib/%.o: src/%.cpp
	$(CC) $(CFLAGS) $< -o $@

#%: src/%.cpp
#	$(CC) $(CFLAGS) $< -o $@

clean:
	rm lib/* $(TARGET)
